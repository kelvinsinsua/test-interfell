package com.testinterfell;

import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.util.Log;

import java.security.Key;
import java.util.Map;
import java.util.HashMap;

import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import android.util.Base64;
import 	java.io.IOException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchProviderException;
import com.facebook.react.bridge.Callback;;

public class EncryptionDesModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    private static final String TAG = "TEST_I";

    public EncryptionDesModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "EncryptionDes";
    }

    @ReactMethod
    public String desEncrypt(String plainRut,Callback successCallback,Callback errorCallback) {

        Log.d(TAG,plainRut);
        try{
            String key = "ionix123456";
            DESKeySpec keySpec = new DESKeySpec(key.getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key2 = keyFactory.generateSecret(keySpec);
            byte[] cleartext = plainRut.getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key2);
            String encryptedRut = Base64.encodeToString(cipher.doFinal(cleartext), Base64.DEFAULT);

            Log.d(TAG, encryptedRut);
            successCallback.invoke(encryptedRut);
        }
        catch(IOException | InvalidKeyException  | InvalidKeySpecException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e){
            Log.d(TAG, "error");
            errorCallback.invoke("Error");
        }
        


        return "Hola";
    }
}