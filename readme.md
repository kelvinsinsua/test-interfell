
## Sobre las pruebas unitarias

Se agregaron pruebas unitarias de renderizado de los dos componentes existented dentro de la app, donde se verifica que los mismo hayan sido montados correctamente y que estén recibiendo las propiedades correctas y funcionando. 

En principio se agregaron estos test para realizar las prubas básicas y que la app por problemas visuales no se rompa o funcione de manera incorrecta. 

Sin embargo se pueden agregar test de integración para verificar las llamadas al API, el encriptado correcto del RUT o DNI, muestra de errores correctamente a través de las alertar, entre otros. Los cuales conllevan un poco mas de tiempo de desarrollo y es ideal que sean diseñados previo al desarrollo del código. 

para realizar la ejecución de las pruebas unitarias ejecutar. 

```bash
yarn run test
```
o
```bash
npm run test
```