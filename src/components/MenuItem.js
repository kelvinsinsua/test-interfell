import React from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions,
  } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';

const window = Dimensions.get("window");

const MenuItem = ({iconName, label, onPress}) => {
    return(
        <View style={styles.container}>
            <TouchableOpacity style={styles.itemContainer} onPress={onPress}>
                <View style={styles.iconContainer}>
                    <Icon name={iconName} size={25} color="#000" />
                </View>
                <View style={styles.labelContainer}>
                    <Text style={styles.label}>{label}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      justifyContent: 'center',
      alignItems: 'center',
      padding: 3
    },
    itemContainer: {
      /* flex: 1,*/
      flexDirection: "column",
      justifyContent: 'center', 
      alignItems: 'center',
      width: window.width * 0.28,
      height: window.width * 0.28,
      backgroundColor: 'rgba(205,205,205,.5)',
      borderRadius: 8,
      padding: 3
    },
    label: {
        textAlign: 'center',
        fontSize: 12
    },
    iconContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    labelContainer: {
        flex: 1
    }
})
export default MenuItem;