import React, {useState} from 'react'
import {
    View,
    ImageBackground,
    StyleSheet,
    Text,
    Alert,
    Dimensions,
    Button,
    SafeAreaView,
    Modal,
    AsyncStorage
  } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';

import MenuItem from '../components/MenuItem.js'
import EncryptionDes from '../utils/EncryptionDes';


const window = Dimensions.get("window");





const Index = () => {

    const [visible, setVisible] = useState(false);
    const [rut, setRut] = useState('');
    const [rutError, setRutError] = useState('');
    const [loading, setLoading] = useState(false);
  
    const onButton1Press = () => {
        setVisible(!visible)
    };

    const onButton2Press = () => {
        AsyncStorage.getItem('user').then(value =>{
            console.log(value)
            if(value){
                axios({
                    method: 'post',
                    url: `https://jsonplaceholder.typicode.com/users `,
                    data: JSON.parse(value)
                  })
                    .then(function (response) {
                        console.log(response)
                        if(response.status == 201){
                            Alert.alert(
                                "Exito",
                                "Usuario creado exitosamente. ID: "+response.data.id,
                                [
                                  {
                                    text: "Cancelar",
                                    onPress: () => {},
                                    style: "cancel"
                                  },
                                  { text: "OK", onPress: () => {} }
                                ],
                                { cancelable: false }
                              );
                        }else{
                            Alert.alert(
                                "Error",
                                "Ha ocurrido un error crear el usuario.",
                                [
                                  {
                                    text: "Cancelar",
                                    onPress: () => {},
                                    style: "cancel"
                                  },
                                  { text: "OK", onPress: () => {} }
                                ],
                                { cancelable: false }
                              );
                        }
                    });
            }else{
                Alert.alert(
                    "Info",
                    "Debe realizar un Pago Fácil antes de ir a la billetera.",
                    [
                      {
                        text: "Cancelar",
                        onPress: () => {},
                        style: "cancel"
                      },
                      { text: "OK", onPress: () => {} }
                    ],
                    { cancelable: false }
                  );
            }
        });
    };

    const accept = () => {
        if(rut.length){
            setRutError('')
            if(rut == "1-9"){
                EncryptionDes.desEncrypt(rut, (encryptedRut) => {
                    axios({
                        method: 'get',
                        url: `https://sandbox.ionix.cl/test-tecnico/search?rut=${encryptedRut}`,
                      })
                        .then(function (response) {
                            AsyncStorage.setItem('user', JSON.stringify(response.data.result.items[1]));
                            Alert.alert(
                                "Hola "+response.data.result.items[1].name,
                                'Email: '+response.data.result.items[1].detail.email+' Teléfono:'+response.data.result.items[1].detail.email,
                                [
                                  {
                                    text: "Cancelar",
                                    onPress: () => cancelModal(),
                                    style: "cancel"
                                  },
                                  { text: "OK", onPress: () => cancelModal() }
                                ],
                                { cancelable: false }
                              );
                        });
                }, (error) => {
                    Alert.alert(
                        "Error",
                        "Ha ocurrido un error al encriptar el RUT.",
                        [
                          {
                            text: "Cancelar",
                            onPress: () => cancelModal(),
                            style: "cancel"
                          },
                          { text: "OK", onPress: () => cancelModal() }
                        ],
                        { cancelable: false }
                      );
                    
                })
            }else{
                Alert.alert(
                    "Error",
                    "RUT inválido.",
                    [
                      {
                        text: "Cancelar",
                        onPress: () => cancelModal(),
                        style: "cancel"
                      },
                      { text: "OK", onPress: () => cancelModal() }
                    ],
                    { cancelable: false }
                  );
            }
            

        }else{
            setRutError('Debe ingresar un RUT válido')
        }
    }

    const changeRut = (text) => {
        if(text.length){
            setRutError('')
            
        }else{
            setRutError('Debe ingresar un RUT válido')
        }
        setRut(text)
    }

    const cancelModal = () => {
        setVisible(false);
        setRut('');
        setRutError('');
    }

    return(
        <SafeAreaView style={styles.container}>
            <Modal visible={visible} transparent animationType="fade">
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent}>
                        <View>
                            <Text>Ingrese su RUT</Text>
                            <TextInput style={styles.input} onChangeText={changeRut}/>
                            {rutError ? <Text style={styles.errorText}>{rutError}</Text> : null}
                        </View>
                        <View style={styles.actionsContainer}>
                            <View style={{marginRight: 5}}>
                                <Button
                                    onPress={cancelModal}
                                    title="Cancelar"
                                    color="red"
                                    style={styles.button}
                                />
                            </View>                            
                            <Button
                                onPress={accept}
                                title="Aceptar"
                                style={styles.button}
                            />
                            
                        </View>
                    </View>
                </View>
                
            </Modal>
            <ImageBackground source={require('../assets/background.jpg')} style={styles.image}>
                <View style={styles.infoContainer}>
                    <View style={styles.userContainer}>
                        <Text style={styles.title}>Bienvenido</Text>
                        <Text style={styles.subtitle}>Francisco</Text>
                    </View>
                    <View style={styles.versionContainer}>
                        <View style={styles.circle}>
                            <Icon name="question" size={20} color="#000" />
                        </View>
                        <View>
                            <Text style={styles.version}>v2.5.4</Text>
                        </View>
                        
                    </View>
                </View>

                <View style={styles.menuContainer}>
                    <View style={styles.menuRow}>
                        <MenuItem iconName="dollar-sign" label="Pago Fácil" onPress={onButton1Press} />
                        <MenuItem iconName="wallet" label="Billetera" onPress={onButton2Press}/>
                        <MenuItem iconName="map-marked-alt" label="Estaciones de Servicios" onPress={() => {}}/>
                    </View>
                    <View style={styles.menuRow}>
                        <MenuItem iconName="tag" label="Mis Beneficios" onPress={() => {}}/>
                        <MenuItem iconName="envelope" label="Mensajes (0)" onPress={() => {}}/>
                        <MenuItem iconName="cog" label="Configuración" onPress={() => {}}/>
                    </View>
                </View>
                <View style={styles.underlineContainer}>
                    <View style={styles.underline}>

                    </View>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column"
    },
    image: {
      flex: 1,
      resizeMode: "cover",
      height: window.height,
      width: '100%'
    },
    title: {
      color: "#FFF",
      fontSize: 24,
      fontWeight: "bold"
    },
    subtitle: {
      color: "#FFF",
      fontSize: 16
    },
    infoContainer: {
        flexDirection: 'row',
        padding: 20,
    },
    userContainer: {
        flex: 1
    },
    versionContainer: {
        flex: 1,
        alignItems: 'flex-end',
        flexDirection: 'column'
    },
    circle: {
        width: 30,
        height: 30,
        backgroundColor: "#FFF",
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 6
    },
    version: {
        color: "#FFF"
    },
    menuContainer: {
        flex: 1,
        padding: 20,
        justifyContent: 'flex-end'
    },
    menuRow: {
        flexDirection: 'row'
    },
    underlineContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    underline: {
        width: '50%',
        borderWidth: 4,
        borderRadius: 2,
        borderColor: "#000"
    },
    modalContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: window.height,
    },
    modalContent: {
        width: '80%',
        height: 150,
        backgroundColor: "#FFF",
        borderRadius: 10,
        padding: 8,
        elevation: 5
    },
    input: {
        borderBottomColor: "#000",
        borderBottomWidth: 1
    },
    actionsContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        flex: 1
    },
    errorText: {
        color: "red"
    }
  });
  

export default Index;