import React from 'react';
import {shallow, mount} from 'enzyme';
import App from './index';
import MenuItem from '../components/MenuItem';
import { TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';


describe('App', () => {
    describe('Rendering App and menus', () => {
        it('should render 6 menu items', () => {
            const wrapper = shallow(<App />)
            expect(wrapper.find(MenuItem).length).toBe(6);
        });
    });

    describe('Render MenuItem', () => {
        it('should call onPress, have a label and a icon.', () => {
            const mockOnPress = jest.fn(); 
            const wrapper = shallow(<MenuItem onPress={mockOnPress} label="test" iconName="wallet" />);
            wrapper.find(TouchableOpacity).first().props().onPress();
            const text = wrapper.find(Text).first();
            const icon = wrapper.find(Icon).first();

            expect(text.props().children).toBe('test');
            expect(icon.props().name).toBe('wallet');
            expect(mockOnPress).toHaveBeenCalled();
            expect(mockOnPress).toHaveBeenCalledTimes(1);
        });
    });
});